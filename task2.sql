use contractors;

-- 2. Запросы с использованием нескольких таблиц.
-- 2.1. Бинарные операции и соединения.
-- 2.1.1. Реализация EXCEPT (MINUS), INTERSECT, UNION.

select employee_id, employee_name from employees where employee_id <= 9 
union 
select EMPLOYEE_ID, EMPLOYEE_NAME from employees where EMPLOYEE_ID >= 7;

select employee_id, employee_name from employees where employee_id <= 9 
union all
select EMPLOYEE_ID, EMPLOYEE_NAME from employees where EMPLOYEE_ID >= 7;

select distinct employee_id, employee_name from employees 
where employee_id <= 9 and employee_id in 
(select employee_id from employees where  EMPLOYEE_ID >= 7); -- intersect emulation

select distinct employee_id, employee_name from employees 
where employee_id <= 9 and employee_id not in 
(select employee_id from employees where  EMPLOYEE_ID >= 7); -- except emulation

-- 2.1.2. Реализация операции деления отношений.