use contractors;

INSERT INTO customers VALUES('Red October','Pushkina st., b. 14');
INSERT INTO customers VALUES('Long Bottom','Lenina st., b. 228');
INSERT INTO customers VALUES('Flat White Roof Ltd.','Marx al., b. 121');
INSERT INTO customers VALUES('Spiders for Everyone','Red Square, h. 1');
INSERT INTO customers VALUES('Bloatflies Inc.','Neverwhere');
INSERT INTO customers VALUES('Burning September','Pushkina st., b. 14');
INSERT INTO customers VALUES('OOO My Defense','10 Let of October st., 19');

INSERT INTO contracts VALUES(NULL, '2012-01-01', 1000);
INSERT INTO contracts VALUES(NULL, '2013-02-02', 2000);
INSERT INTO contracts VALUES(NULL, '2014-03-04', 3000);
INSERT INTO contracts VALUES(NULL, '2005-06-02', 4000);
INSERT INTO contracts VALUES(NULL, '2013-02-12', 7000);
INSERT INTO contracts VALUES(NULL, '2009-12-23', 66000);
INSERT INTO contracts VALUES(NULL, '2008-05-17', 89000);
INSERT INTO contracts VALUES(NULL, '2002-02-02', 32000);
INSERT INTO contracts VALUES(NULL, '2018-09-16', 2000);
INSERT INTO contracts VALUES(NULL, '2019-09-25', 1111000);

INSERT INTO customers_has_contracts VALUES('Red October',1);
INSERT INTO customers_has_contracts VALUES('Long Bottom',2);
INSERT INTO customers_has_contracts VALUES('Flat White Roof Ltd.',3);
INSERT INTO customers_has_contracts VALUES('Spiders for Everyone',4);
INSERT INTO customers_has_contracts VALUES('Bloatflies Inc.',5);
INSERT INTO customers_has_contracts VALUES('OOO My Defense',6);
INSERT INTO customers_has_contracts VALUES('Red October',7);
INSERT INTO customers_has_contracts VALUES('Spiders for Everyone',8);
INSERT INTO customers_has_contracts VALUES('OOO My Defense',9);
INSERT INTO customers_has_contracts VALUES('Long Bottom',10);

INSERT INTO departments VALUES(NULL, 'Loader');
INSERT INTO departments VALUES(NULL, 'Fixer');
INSERT INTO departments VALUES(NULL, 'Builder');
INSERT INTO departments VALUES(NULL, 'Scraper');
INSERT INTO departments VALUES(NULL, 'Biter');
INSERT INTO departments VALUES(NULL, 'Viper');

INSERT INTO employees VALUES(NULL,'Alexeo Bajdakov',1);
INSERT INTO employees VALUES(NULL,'Ivan Ivanov',1);
INSERT INTO employees VALUES(NULL,'Michael Machaelov',2);
INSERT INTO employees VALUES(NULL,'Roman Romanov',2);
INSERT INTO employees VALUES(NULL,'Dimitry Dimitriev',3);
INSERT INTO employees VALUES(NULL,'Sasha Pervih',4);
INSERT INTO employees VALUES(NULL,'Danila Bagrov',4);
INSERT INTO employees VALUES(NULL,'Kostya Drevnev',4);
INSERT INTO employees VALUES(NULL,'Carl Johnson',5);
INSERT INTO employees VALUES(NULL,'Valera Molotok',6);
INSERT INTO employees VALUES(NULL,'Ded Maxim',2);
INSERT INTO employees VALUES(NULL,'Carl Johnson',4);
INSERT INTO employees VALUES(NULL,'Valera Molotok',3);
INSERT INTO employees VALUES(NULL,'Ded Maxim',5);

INSERT INTO heads VALUES(1,1);
INSERT INTO heads VALUES(2,11);
INSERT INTO heads VALUES(3,5);
INSERT INTO heads VALUES(4,7);
INSERT INTO heads VALUES(5,9);
INSERT INTO heads VALUES(6,10);

INSERT INTO executors VALUES(1,2);
INSERT INTO executors VALUES(1,7);
INSERT INTO executors VALUES(2,5);
INSERT INTO executors VALUES(3,3);
INSERT INTO executors VALUES(4,11);
INSERT INTO executors VALUES(5,10);
INSERT INTO executors VALUES(6,4);
INSERT INTO executors VALUES(7,9);
INSERT INTO executors VALUES(8,1);
INSERT INTO executors VALUES(9,2);
INSERT INTO executors VALUES(10,3);
INSERT INTO executors VALUES(10,6);
INSERT INTO executors VALUES(6,9);