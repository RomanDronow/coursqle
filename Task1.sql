use contractors;

-- 1. Запросы с использованием одной таблицы.
-- 1.1. Выборка без использования WHERE. ALL, DISTINCT. Использование фразы CASE.

SELECT DEPARTMENT_NAME FROM departments;
SELECT ALL CUSTOMER_NAME FROM customers;
SELECT DISTINCT EMPLOYEE_NAME FROM employees;
SELECT CASE 
	WHEN DEPARTMENTS_DEPARTMENT_ID <=3 THEN 'less than three'
    WHEN DEPARTMENTS_DEPARTMENT_ID > 3 THEN 'greater than three'
    END
    FROM employees;
    
-- 1.2. Выборка вычисляемых значений. Использование псевдонимов таблиц.

-- 1.3. Синтаксис фразы WHERE. BETWEEN, IS [NOT] NULL, LIKE, UPPER, LOWER. IN, EXISTS.

SELECT * FROM contracts WHERE CONTRACT_COST BETWEEN 1000 AND 10000;
SELECT * FROM customers WHERE CUSTOMER_ADDRESS LIKE '% st.%';
SELECT UPPER(EMPLOYEE_NAME) AS UpperEmpName FROM employees;
SELECT EMPLOYEE_NAME FROM employees WHERE DEPARTMENTS_DEPARTMENT_ID IN('1','5');

-- 1.4. Выборка с упорядочением. ORDER BY, ASC, DESC.

select * from customers order by customer_address asc;
select * from employees order by employee_name desc;

-- 1.5. Агрегирование данных. Агрегатные SQL-функции (COUNT, SUM, AVG, MIN, MAX).

select count(contract_cost) from contracts;
select sum(contract_cost) from contracts;
select avg(contract_cost) from contracts;
select min(contract_cost) from contracts;
select max(contract_cost) from contracts;

-- 1.6. Агрегирование данных без и с использованием фразы GROUP BY. Фраза HAVING.

select count(customer_name), customer_address from customers group by customer_address;